### Description
An application to find differences between two images.

![Example](/example.png?raw=true "Example")

### Run pre-built jar

```
java -jar image-diff.jar [<input-image-path-1> <input-image-path-2> <output-image-path> <radius> <line-width>]
java -jar image-diff.jar image1.png image2.png output.jpg 20 5
java -jar image-diff.jar
```

### Default values
  - `<input-image-path-1> - image1.png`
  - `<input-image-path-2> - image2.png`
  - `<output-image-path> - output.jpg`
  - `<radius> - 10`
  - `<line-width> - 5`


### Build jar

`sbt assembly`
