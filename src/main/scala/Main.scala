import java.awt.{BasicStroke, Color}
import java.awt.image.BufferedImage
import java.io.File
import java.nio.ByteBuffer
import java.nio.file.{Files, Paths}
import javax.imageio.ImageIO

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object Main {
  def main(args: Array[String]): Unit = {
    args.toSeq match {
      case Seq() => process("image1.png", "image2.png", "output.jpg", 10, 5)
      case Seq("help") | Seq("--help") => printHelp()
      case Seq(inputPath1, inputPath2, outputPath, radius, lineWidth) =>
        process(inputPath1, inputPath2, outputPath, radius.toInt, lineWidth.toInt)
      case _ => printHelp()
    }
  }

  def printHelp(): Unit = {
    print(
      """Usage:
        |  [<input-image-path-1> <input-image-path-2> <output-image-path> <radius>] - process image
        |Default values:
        |  <input-image-path-1> - image1.png
        |  <input-image-path-2> - image2.png
        |  <output-image-path> - output.jpg
        |  <radius> - 10
        |  <line-width> - 5
      """.stripMargin)
  }

  def process(inputPath1: String, inputPath2: String, outputPath: String, radius: Int, lineWidth: Int): Unit = {
    val image1 = ImageIO.read(ImageIO.createImageInputStream(Files.newInputStream(Paths.get(inputPath1))))
    val image2 = ImageIO.read(ImageIO.createImageInputStream(Files.newInputStream(Paths.get(inputPath2))))
    val w = Math.min(image1.getWidth, image2.getWidth)
    val h = Math.min(image1.getHeight, image2.getHeight)
    val wSm = w / radius + 1
    val hSm = h / radius + 1
    println("wSm: " + wSm + " hSm: " + hSm + " w: " + w + " h: " + h + " radius: " + radius)
    val points = ByteBuffer.allocateDirect(wSm * hSm)
    // scale down, "picking max difference in the radius"
    for (y <- 0 until h) {
      for (x <- 0 until w) {
        var c1 = image1.getRGB(x, y) // inefficient, convert image color format and make ByteBuffer with color data to iterate faster
        var c2 = image2.getRGB(x, y)
        // computing grayscale
        c1 = ((c1 >> 16) & 0xFF) + ((c1 >> 8) & 0xFF) + (c1 & 0xFF) // assuming ARGB color format, like getRGB() returns
        // storing the difference immediately to the c2
        c2 = ((c2 >> 16) & 0xFF) + ((c2 >> 8) & 0xFF) + (c2 & 0xFF)
        // threshold = 255 * 3 * 10%
        if ((c2 - c1) < -255 * 3 / 10 || (c2 - c1) > 255 * 3 / 10) {
          points.put(y / radius * wSm + x / radius, 1)
        }
      }
    }
    // TODO exclude provided rects by filling 'points' with zeros
    /*
    // expand
    val pointsExpanded = ByteBuffer.allocateDirect(wSm * hSm)
    for (y <- 1 until hSm - 1) {
      for (x <- 1 until wSm - 1) {
        if (points.get(y * wSm + x) > 0) {
          // fill 8 cells around
          pointsExpanded.put((y - 1) * wSm + (x - 1), 1)
          pointsExpanded.put((y - 1) * wSm + x, 1)
          pointsExpanded.put((y - 1) * wSm + (x + 1), 1)
          pointsExpanded.put(y * wSm + (x - 1), 1)
          pointsExpanded.put(y * wSm + x, 1)
          pointsExpanded.put(y * wSm + (x + 1), 1)
          pointsExpanded.put((y + 1) * wSm + (x - 1), 1)
          pointsExpanded.put((y + 1) * wSm + x, 1)
          pointsExpanded.put((y + 1) * wSm + (x + 1), 1)
        }
      }
    }
    points = pointsExpanded
    */
    // find bounding boxes
    // >  zero-fill image contour along bounds to prevent overflow
    for (x <- 0 until wSm) {
      points.put(0 * wSm + x, 0)
    }
    for (x <- 0 until wSm) {
      points.put((hSm - 1) * wSm + x, 0)
    }
    for (y <- 0 until hSm) {
      points.put(y * wSm + 0, 0)
    }
    for (y <- 0 until hSm) {
      points.put(y * wSm + wSm - 1, 0)
    }
    val boxes = new ArrayBuffer[Array[Int]]()
    for (y <- 1 until hSm) {
      for (x <- 1 until wSm) {
        if (points.get(y * wSm + x) > 0) {
          val box = findBoundingBox(x, y, points, wSm, hSm) // x1, y1, x2, y2
          boxes += box
          // removing points that are in the box
          for (y <- box(1) to box(3)) {
            for (x <- box(0) to box(2)) {
              points.put(y * wSm + x, 0)
            }
          }
        }
      }
    }

    val outputImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB)
    val outputGraphics = outputImage.createGraphics()
    outputGraphics.drawImage(image2, 0, 0, Color.BLACK, null)
    outputGraphics.setColor(Color.RED)
    outputGraphics.setStroke(new BasicStroke(lineWidth))
    // draw bounding boxes
    boxes.foreach(box => outputGraphics.drawRect(box(0) * radius, box(1) * radius, (box(2) - box(0) + 1) * radius, (box(3) - box(1) + 1) * radius))
    // draw points
    /*for (y <- 0 until hSm - 1) {
      for (x <- 0 until wSm - 1) {
        if (points.get(y * wSm + x) > 0) {
          outputGraphics.drawRect(x * radius, y * radius, radius, radius)
        }
      }
    }*/
    outputGraphics.dispose()
    ImageIO.write(outputImage, "jpg", new File(outputPath))
  }

  /* Assuming that either startX or startY is equal to the resulting minX or minY correspondingly */
  def findBoundingBox(startX: Int, startY: Int, points: ByteBuffer, w: Int, h: Int): Array[Int] = {
    var minX: Int = startX
    var minY: Int = startY
    var maxX: Int = startX
    var maxY: Int = startY
    var curX: Int = startX
    var curY: Int = startY
    // find contour using flood fill algorithm
    val stack = mutable.ArrayStack(Array(minX, minY))
    do {
      val cur = stack.pop()
      curX = cur(0)
      curY = cur(1)
      if (curX < minX) {
        minX = curX
      }
      if (curY < minY) {
        minY = curY
      }
      if (curX > maxX) {
        maxX = curX
      }
      if (curY > maxY) {
        maxY = curY
      }
      // add ok (1) points to the stack, mark them as passed (2)
      if (points.get((curY - 1) * w + (curX + 1)) == 1) {
        points.put((curY - 1) * w + (curX + 1), 2)
        stack.push(Array(curX + 1, curY - 1))
      } else if (points.get(curY * w + (curX + 1)) == 1) {
        points.put(curY * w + (curX + 1), 2)
        stack.push(Array(curX + 1, curY))
      } else if (points.get((curY + 1) * w + (curX + 1)) == 1) {
        points.put((curY + 1) * w + (curX + 1), 2)
        stack.push(Array(curX + 1, curY + 1))
      } else if (points.get((curY + 1) * w + curX) == 1) {
        points.put((curY + 1) * w + curX, 2)
        stack.push(Array(curX, curY + 1))
      } else if (points.get((curY + 1) * w + (curX - 1)) == 1) {
        points.put((curY + 1) * w + (curX - 1), 2)
        stack.push(Array(curX - 1, curY + 1))
      } else if (points.get(curY * w + (curX - 1)) == 1) {
        points.put(curY * w + (curX - 1), 2)
        stack.push(Array(curX - 1, curY))
      } else if (points.get((curY - 1) * w + (curX - 1)) == 1) {
        points.put((curY - 1) * w + (curX - 1), 2)
        stack.push(Array(curX - 1, curY - 1))
      } else if (points.get((curY - 1) * w + curX) == 1) {
        points.put((curY - 1) * w + curX, 2)
        stack.push(Array(curX, curY - 1))
      }
    } while (stack.nonEmpty)

    Array(minX, minY, maxX, maxY)
  }
}